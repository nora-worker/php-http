<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\HTTP;

use Nora\Core\Component\Component;
use Nora\Core\Util\Collection\Hash;
use Nora\Core\Util\Text;

/**
 * HTTP Client
 *
 * - 内部的にcurlを使う
 */
class Client extends Component
{
    const AUTOREFERER    = 100;
    const REFERER        = 200;
    const RETURNTRANSFER = 1;
    const TIMEOUT        = 2;
    const HTTPHEADER     = 3;
    const HEADER         = 4;
    const HEADER_OUT     = 410;
    const HEADERFUNCTION = 420;
    const SSL_VERIFYPEER = 5;
    const SSL_VERIFYHOST = 6;
    const URL            = 7;
    const FOLLOWLOCATION = 8;
    const MAXREDIRS      = 81;
    const USERAGENT      = 9;
    const COOKIE         = 10;
    const COOKIEJAR      = 11;
    const COOKIEFILE     = 12;
    const POST           = 20;
    const POSTFIELDS     = 21;
    const VERBOSE        = 99;
    const CUSTOMREQUEST  = 98;
    const HTTPAUTH       = 80;
    const USERPWD        = 81;
    const AUTH_BASIC     = CURLAUTH_BASIC;

    protected function initComponentImpl( )
    {
        $this->options( )->set([
            'RETURNTRANSFER' => 1,
            'TIMEOUT'        => 2,
            'HEADER'         => 1,
            'SSL_VERIFYPEER' => false,
            'SSL_VERIFYHOST' => 0,
            'HEADER_OUT'     => true,
            'AUTOREFERER'    => true,
        ]);

        //$this->headers()->set([
        //    'Accept' => '*/*',
        //   'Connection' => 'Keep-Alive',
        //  'Keep-Alive' => 115
        //]);
    }

    public function setUseCookie( )
    {
        $this->options( )->set([
            'HEADERFUNCTION' => function ($input, $line){
                if(preg_match('/^Set-Cookie:\s*([^;]*)/mi', $line, $m))
                {
                    list($name, $value) = explode('=', $m[1], 2);
                    $this->setCookie($name, $value);
                }
                return strlen($line);
            }
        ]);

        $this->setUseCookieFile(tmpfile());
    }

    public function setUseCookieFile($file)
    {
        $this->options( )->set([
            'COOKIEJAR' => $file,
            'COOKIEFILE' => $file
        ]);
        return $this;
    }

    public function setAuthBasic($cred)
    {
        $this->options( )->set([
            'USERPWD' => $cred,
        ]);
        return $this;
    }

    public function setFollowLocation($max = 5)
    {
        $this->options( )->set([
            'FOLLOWLOCATION' => true,
            'MAXREDIRS' => $max
        ]);
        return $this;
    }

    public function setCookie($name, $value)
    {
        $this->fire('recive.cookie', [
            'name' => $name,
            'value' => $value
        ]);
        return $this;
    }


    public function setOption($name, $value = null)
    {
        if (is_array($name)) {
            foreach($name as $k=>$v)
            {
                $this->setOption($k, $v);
            }
            return $this;
        }

        if (method_exists($this, $method = 'set'.Text::toCamel($name) ))
        {
            $this->{$method}($value);
        }
        else
        {
            $this->options()->set(
                constant('self::'.strtoupper($name)),
                $value
            );
        }
        return $this;
    }

    public function setUseFakeAgent($agent)
    {
        if ($agent === null || !is_string($agent))
        {
            $agent = 'Mozilla/6.0 (Windows; U; Windows NT 6.0; ja; rv:1.9.1.1) ';
            $agent.= 'Gecko/20090715 Firefox/3.5.1 (.NET CLR 3.5.30729)';
        }

        $this->options()->set(self::USERAGENT, $agent);
    }

    public function bootOptions( )
    {
        return new Hash();
    }

    public function bootHeaders( )
    {
        return new Hash();
    }

    public function addHeader($name, $value = null)
    {
        if (is_array($name))
        {
            foreach($name as $k=>$v) $this->addHeader($k, $v);
            return $this;
        }
        $this->headers()->set($name, $value);
        return $this;
    }

    public function initHandler( )
    {
        return new Curl($this);
    }

    /**
     * Get Access
     */
    public function get ($url, $datas = [], $headers = [], $options = [], $doExecute = true)
    {
        $url = (is_array($datas) && !empty($datas)) ?
            $url.'?'.http_build_query($datas) :
            $url;

        $handler = $this->initHandler();
        $options[self::URL] = $url;
        $options[self::HTTPHEADER] = $this
            ->headers()
            ->set($headers)
            ->toArray();
        $handler->setOpt($options);

        if ($doExecute) {
            $res = $handler->exec();
            return $res;
        }
        return $handler;
    }

    /**
     * Post Access
     */
    public function post ($url, $datas = [], $headers = [], $options = [], $doExecute = true)
    {
        $datas = (is_array($datas) && !empty($datas)) ?
            http_build_query($datas) :
            $datas;


        $handler = $this->initHandler();
        $options[self::URL] = $url;
        $options[self::HTTPHEADER] = $this
            ->headers()
            ->set($headers)
            ->toArray();
        $options[self::POST] = true;
        $options[self::POSTFIELDS] = $datas;
        $handler->setOpt($options);
        if ($doExecute) {
            $res = $handler->exec();
            return $res;
        }
        return $handler;
    }

    /**
     * Put
     */
    public function put ($url, $datas = [], $options = [], $doExecute = true)
    {
        $datas = (is_array($datas) && !empty($datas)) ?
            http_build_query($datas) :
            $datas;

        $handler = $this->initHandler();
        $options[self::URL] = $url;
        $options[self::CUSTOMREQUEST] = 'PUT';
        $options[self::POSTFIELDS] = $datas;
        $options[self::HTTPHEADER] = $this
            ->headers()
            ->set(isset($options['HTTPHEADER']) ? $options['HTTPHEADER']: [])
            ->toArray();
        $handler->setOpt($options);

        if ($doExecute) {
            $res = $handler->exec();
            return $res;
        }
        return $handler;
    }


    /**
     * マルチリクエストハンドラを取得
     */
    public function multi( )
    {
        return new Multi($this);
    }
}
