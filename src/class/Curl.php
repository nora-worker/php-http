<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\HTTP;

use Nora\Core\Component\Component;
use Nora\Core\Util\Collection\Hash;
use Nora\Core\Util\Text;

/**
 * Curlラッパー
 */
class Curl
{
    private $_client;
    private $_curl;
    private static $map =
        [
            Client::AUTOREFERER    => CURLOPT_AUTOREFERER,
            Client::REFERER        => CURLOPT_REFERER,
            Client::RETURNTRANSFER => CURLOPT_RETURNTRANSFER,
            Client::TIMEOUT        => CURLOPT_TIMEOUT,
            Client::HTTPHEADER     => CURLOPT_HTTPHEADER,
            Client::HEADER         => CURLOPT_HEADER,
            Client::HEADER_OUT     => CURLINFO_HEADER_OUT,
            Client::HEADERFUNCTION => CURLOPT_HEADERFUNCTION,
            Client::SSL_VERIFYPEER => CURLOPT_SSL_VERIFYPEER,
            Client::SSL_VERIFYHOST => CURLOPT_SSL_VERIFYHOST,
            Client::URL            => CURLOPT_URL,
            Client::FOLLOWLOCATION => CURLOPT_FOLLOWLOCATION,
            Client::MAXREDIRS      => CURLOPT_MAXREDIRS,
            Client::USERAGENT      => CURLOPT_USERAGENT,
            Client::COOKIE         => CURLOPT_COOKIE,
            Client::COOKIEJAR      => CURLOPT_COOKIEJAR,
            Client::COOKIEFILE     => CURLOPT_COOKIEFILE,
            Client::POST           => CURLOPT_POST,
            Client::POSTFIELDS     => CURLOPT_POSTFIELDS,
            Client::VERBOSE        => CURLOPT_VERBOSE,
            Client::CUSTOMREQUEST  => CURLOPT_CUSTOMREQUEST,
            Client::HTTPAUTH       => CURLOPT_HTTPAUTH,
            Client::USERPWD        => CURLOPT_USERPWD
        ];

    public function __construct($client)
    {
        $this->_client = $lient;
        $this->_curl   = curl_init();

        $this->setOpt([
            Client::HEADER_OUT => true
        ]);

        // クライアントのオプションを引き継ぐ
        foreach($client->options()->toArray() as $k=>$v)
        {
            $this->setOpt($k, $v);
        }

    }

    public function curl()
    {
        return $this->_curl;
    }

    public function setOpt($key, $value = null)
    {
        if (is_array($key))
        {
            foreach($key as $k=>$v) $this->setOpt($k, $v);
            return $this;
        }

        if (is_string($key)) {
            $key = constant(__namespace__.'\Client::'.$key);
        }

        if ($key === Client::HTTPHEADER)
        {
            $new_value = [];
            foreach($value as $k => $v)
            {
                $new_value[] = "$k: $v";
            }
            $value = $new_value;
        }



        curl_setopt($this->_curl, $c = self::$map[$key], $value);
        return $this;
    }

    public function exec( )
    {
        $res    = curl_exec($this->_curl);
        $info   = curl_getinfo($this->_curl);
        $size   = $info['header_size'];
        $header = substr($res, 0, $size);
        $body   = substr($res, $size);


        curl_close($this->_curl);
        return new Response($header, $body, $info);
    }

}

