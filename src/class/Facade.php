<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\HTTP;

use Nora\Core\Module;

/**
 * HTTP
 */
class Facade implements Module\ModuleIF
{
    use Module\Modulable;

    protected function initModuleImpl( )
    {
        $this->setHelper([
            # HTTP Clientを取得する
            'Client' => ['scope', function ($s, $options = []) {
                $client = new Client();
                $client->setScope($s->newScope());
                $client->setOption($options);
                return $client;
            }]
        ]);
    }
}
