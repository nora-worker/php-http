<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\HTTP;

use Nora\Core\Component\Component;
use Nora\Core\Util\Collection\Hash;
use Nora\Core\Util\Text;

/**
 * Curl Multi ラッパー
 */
class Multi
{
    private $_client;
    private $_handler;
    private $_handlers;

    public function __construct($client)
    {
        $this->_client = $client;
        $this->_handler = curl_multi_init( );
    }

    public function get ($url, $datas = [], $headers= [], $options = [])
    {
        $handler = $this->_client->get($url, $datas, $headers, $options, false);
        curl_multi_add_handle($this->_handler, $handler->curl());
        return $this;
    }

    public function post ($url, $datas = [], $headers = [], $options = [])
    {
        $handler = $this->_client->post($url, $datas, $headers, $options, false);
        curl_multi_add_handle($this->_handler, $handler->curl());
        return $this;
    }

    public function put ($url, $datas = [], $headers = [], $options = [])
    {
        $handler = $this->_client->put($url, $datas, $headers, $options, false);
        curl_multi_add_handle($this->_handler, $handler->curl());
        return $this;
    }
    public function exec( )
    {
        $active = null;
        do {
            $mrc = curl_multi_exec($this->_handler, $active);
        } while($mrc === CURLM_CALL_MULTI_PERFORM);

        while($active && $mrc === CURLM_OK) {
            if (curl_multi_select($this->_handler) === -1)
            {
                usleep(100);
            }

            do {
                $mrc = curl_multi_exec($this->_handler, $active);
            } while($mrc === CURLM_CALL_MULTI_PERFORM);


            while(($multi_info = curl_multi_info_read($this->_handler)) !== false)
            {
                if ($multi_info["result"] === CURLE_OK){
                    $res    = curl_multi_getcontent($multi_info['handle']);
                    $info    = curl_getinfo($multi_info['handle']);
                    $size   = $info['header_size'];
                    $header = substr($res, 0, $size);
                    $body   = substr($res, $size);
                    yield new Response(
                        substr($res, 0, $size),
                        substr($res, $size),
                        $info
                    );
                    curl_multi_remove_handle($this->_handler, $multi_info['handle']);
                }
            }
        }

        if ($mrc !== CURLM_OK)
        {
            throw new \Exception("読み込みエラー");
        }
    }

}

