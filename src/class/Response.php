<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\HTTP;


/**
 * Curlラッパー
 */
class Response
{
    private $_header;
    private $_body;
    private $_info;

    /**
     * コンストラクタ
     */
    public function __construct($header, $body, $info)
    {
        $this->_header = $header;
        $this->_body = $body;
        $this->_info = $info;
    }

    public function getHeader( )
    {
        return $this->_header;
    }
    public function getBody( )
    {
        return $this->_body;
    }

    public function getInfo($name = null)
    {
        if ($name === null) return $this->info;
        return $this->_info[$name];
    }

    public function __toString( )
    {
        return (string) $this->getInfo('request_header')."\n".$this->getHeader()."\n".$this->getBody();
    }

    public function status( )
    {
        return $this->getInfo('http_code');
    }

    public function clear( )
    {
        $this->_body = $this->_header = '';
        $this->_info = [];
    }
}
