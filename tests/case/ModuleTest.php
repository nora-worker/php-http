<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\HTTP;

use Nora;

class ModuleTest extends \PHPUnit_Framework_TestCase
{
    public function testLoad ( )
    {
        $res = Nora::HTTP_Client( )->get('http://mmizui.com');

        $this->assertEquals(200, $res->status());
    }

    public function testClient( )
    {
        $res = Nora::HTTP_Client(['use_fake_agent' => true])->get('http://google.com');
        //echo $res;
        $this->assertEquals(302, $res->status());

        $res = Nora::HTTP_Client([
            'use_fake_agent' => true,
            'FOLLOWLOCATION' => true
        ])->get('http://google.com');

        $this->assertEquals(200, $res->status());
        //echo $res;

        // マルチリクエストのテスト
        $rs = Nora::HTTP_Client()->multi( )
            ->get('https://github.com/hoge')
            ->get('http://google.com')
            ->get('http://yahoo.co.jp')
            ->get('https://google.com')
            ->get('https://google.com')
            ->get('https://google.com')
            ->get('https://google.com')
            ->get('https://google.com')
            ->get('https://google.com')
            ->get('https://google.com')
            ->get('https://google.com')
            ->get('https://google.com')
            ->get('https://google.com')
            ->get('https://google.com')
            ->get('https://google.com')
            ->get('https://google.com')
            ->get('https://google.com')
            ->exec()
            ;

        foreach($rs as $r)
        {
            printf("%s:%s\n", $r->getInfo('url'),$r->getInfo('http_code'));
        }

        var_Dump(Nora::HTTP_Client( )->get('http://google.com', [
            'a' => 'b'
        ])->getInfo('url'));

        $client = Nora::HTTP_Client([
            'use_fake_agent' => true,
            'follow_location' => 10,
            'use_cookie' => true,
        ]);

        $client->observe(function ($e) use ($cookie){
            if ($e->match('recive.cookie'))
            {
                var_dump($e->name, $e->value);
            }
        });

        $client->get('http://mmizui.com/admin');
        $client->get('http://mmizui.com/admin');
        $client->get('http://mmizui.com/admin');
        $r = $client->put('http://mmizui.com/sendmail', [
            'name' => 'hajime',
            'mail' => 'hajime@avap.co.jp',
            'message' => 'hoge'
        ], [
            'REFERER' => 'http://mmizui.com'
        ]);

        vaR_dump( json_decode($r) );
    }
}
